#!/bin/sh

#good mirrors

pacman -Syy

pacman -S reflector --noconfirm

reflector --latest 200 --protocol https --sort rate --save /etc/pacman.d/mirrorlist

#set system clock

timedatectl set-ntp true

#partition with making an efi partition

parted --script /dev/sda \
	mklabel gpt \
	mkpart primary fat32 1MiB 2MiB \
	set 1 bios_grub on \
	mkpart primary linux-swap 2MiB 4GiB \
	mkpart primary ext4 4GiB 100%

mkswap /dev/sda2 
swapon /dev/sda2
mkfs.ext4 /dev/sda3

mount /dev/sda3 /mnt

#installing things

pacstrap /mnt base linux linux-firmware neovim man-pages

genfstab -U /mnt >> /mnt/etc/fstab

#Downloads next part
curl -LO https://gitlab.com/itai33/arch-installer/raw/master/install-chroot.sh
cp install-chroot.sh /mnt/install-chroot.sh

arch-chroot /mnt bash -x install-chroot.sh

