#!/bin/sh

ln -sf /usr/share/zoneinfo/Asia/Jerusalem /etc/localtime

hwclock --systohc

sed -i 's/#en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/g' /etc/locale.gen

locale-gen

echo "LANG=en_US.UTF-8" > /etc/locale.conf

echo "VM" > /etc/hostname

#maybe add thing to /etc/hosts?

#adding a password
echo "root:root" | chpasswd

pacman -S grub dhcpcd --noconfirm

grub-install --target=i386-pc /dev/sda 

grub-mkconfig -o /boot/grub/grub.cfg

curl -LO https://gitlab.com/itai33/arch-installer/raw/master/post-install.sh

exit

reboot
